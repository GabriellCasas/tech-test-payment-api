﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TechTestPaymentApi.Context;
using TechTestPaymentApi.Entities;

namespace TechTestPaymentApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController (VendaContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Método para criação de nova venda
        /// </summary>
        /// <param name="venda">Objeto da classe Venda, com os dados da nova venda</param>
        /// <response code="201">Venda criada com sucesso</response>
        /// <response code="400">A venda deve conter no mínimo um produto para ser realizada</response>
        [HttpPost]
        public IActionResult Create(Venda venda)
        {
            if (venda.Produtos.ToList().Count == 0)
                return BadRequest("A venda deve conter no mínimo um produto para ser realizada");

            _context.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
        }

        /// <summary>
        /// Método para obter venda pelo ID
        /// </summary>
        /// <param name="id">ID da venda a ser obtida</param>
        /// <response code="200">Retorna objeto da classe Venda, com os dados da venda</response>
        /// <response code="404">Venda não encontrada</response>
        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Include(x => x.Produtos).Include(x => x.Vendedor).SingleOrDefault(x => x.Id == id);

            if (venda == null)
                return NotFound();

            var vendaDTO = new Venda
            {
                Id = venda.Id,
                IdPedido = venda.IdPedido,
                DataVenda = venda.DataVenda,
                Status = venda.Status,
                Produtos = venda.Produtos.Select(p => new Produto { Id = p.Id, Nome = p.Nome, Descricao = p.Descricao, Preco = p.Preco }).ToList(),
                Vendedor = venda.Vendedor
            };

            return Ok(vendaDTO);
        }

        /// <summary>
        /// Método para atualizar status de venda
        /// </summary>
        /// <param name="id">ID da venda a ser atualizada</param>
        /// <param name="novoStatus">Novo status da venda</param>
        /// <response code="200">Status da venda atualizado com sucesso</response>
        /// <response code="400">Transição de status inválida.</response>
        /// <response code="404">Venda não encontrada</response>
        [HttpPut("{id}")]
        public IActionResult AtualizarStatus(int id, EnumStatus novoStatus)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
                return NotFound();

            switch (venda.Status)
            {
                case EnumStatus.AguardandoPagamento:
                    if (novoStatus != EnumStatus.PagamentoAprovado && novoStatus != EnumStatus.Cancelada)
                        return BadRequest("Transição de status inválida.");
                    break;
                case EnumStatus.PagamentoAprovado:
                    if (novoStatus != EnumStatus.EnviadoTransportadora && novoStatus != EnumStatus.Cancelada)
                        return BadRequest("Transição de status inválida.");
                    break;
                case EnumStatus.EnviadoTransportadora:
                    if (novoStatus != EnumStatus.Entregue)
                        return BadRequest("Transição de status inválida.");
                    break;
                case EnumStatus.Entregue:
                case EnumStatus.Cancelada:
                    return BadRequest("Não é possível alterar o status desta venda.");
            }

            venda.Status = novoStatus;
            _context.SaveChanges();

            return Ok();
        }
    }
}
