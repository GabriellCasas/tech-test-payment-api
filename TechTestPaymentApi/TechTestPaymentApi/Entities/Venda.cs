﻿namespace TechTestPaymentApi.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public Vendedor Vendedor { get; set; }
        public DateTime DataVenda { get; set; } 
        public int IdPedido { get; set; }
        public List<Produto> Produtos { get; set; }
        public EnumStatus Status { get; set; }
    }
}
