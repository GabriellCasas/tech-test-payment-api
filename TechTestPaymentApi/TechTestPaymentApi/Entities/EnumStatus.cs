namespace TechTestPaymentApi.Entities
{
    public enum EnumStatus
    { 
        AguardandoPagamento,
        PagamentoAprovado,
        Cancelada,
        EnviadoTransportadora,
        Entregue
    }
}