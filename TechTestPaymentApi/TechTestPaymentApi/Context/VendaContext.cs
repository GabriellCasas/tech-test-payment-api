﻿using Microsoft.EntityFrameworkCore;
using TechTestPaymentApi.Entities;

namespace TechTestPaymentApi.Context
{
    public class VendaContext : DbContext
    {
        public VendaContext(DbContextOptions<VendaContext> options) : base(options)
        {

        }

        public DbSet<Venda> Vendas { get; set; }
    }
}
